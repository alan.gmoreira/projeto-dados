package br.com.itau;

import java.util.Random;

public class Dado {

    private int tamanhoDado;
    private int valorJogado;

    public int getTamanhoDado() {
        return tamanhoDado;
    }

    public void setTamanhoDado(int tamanhoDado) {
        this.tamanhoDado = tamanhoDado;
    }

    public int getValorJogado() {
        return valorJogado;
    }

    public void setValorJogado(int valorJogado) {
        this.valorJogado = valorJogado;
    }

    public Dado(int tamanhoDado) {
        this.tamanhoDado = tamanhoDado;
    }

    public Dado(int tamanhoDado, boolean jogar) {
        this.tamanhoDado = tamanhoDado;
        if (jogar)
            this.Jogar();
    }

    public int Jogar()
    {
        Random valor = new Random();
        valorJogado = valor.nextInt(this.tamanhoDado) + 1;
        return valorJogado;
    }
}
