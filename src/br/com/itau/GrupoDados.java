package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class GrupoDados {
    private List<Dado> dados;

    public GrupoDados(List<Dado> dados) {
        this.dados = dados;
    }

    public List<Dado> getDados() {
        return dados;
    }

    public void setDados(List<Dado> dados) {
        this.dados = dados;
    }

    public int getSomaDadod() {
        int soma = 0;

        for (Dado d : dados) {
            soma += d.getValorJogado();
        }
        return soma;
    }

    public void JogarDados(int qtdDados, int tamanhoDados) {
        for (int i = 0; i < qtdDados; i++) {
            this.dados.add(new Dado(tamanhoDados, true));
        }

    }

    @Override
    public String toString() {
        String retorno = "";

        for (Dado d : this.dados) {
            retorno += d.getValorJogado() + ", ";
        }
        retorno += this.getSomaDadod();
        return  retorno;
    }
}
