package br.com.itau;

import java.util.List;

public class Tabuleiro {
    private List<Jogador> jogadores;

    public Tabuleiro(List<Jogador> jogadores) {
        this.jogadores = jogadores;
    }

    public List<Jogador> getJogadores() {
        return jogadores;
    }

    public void setJogadores(List<Jogador> jogadores) {
        this.jogadores = jogadores;
    }

    public Jogador retornaJogadorVencedor() throws Exception {
        Jogador vencedor = null;
        for (Jogador j: this.jogadores) {
            if (vencedor == null)
                vencedor = j;
            else if (j.getPontuacaoTotal() > vencedor.getPontuacaoTotal())
                vencedor = j;
        }

        return vencedor;
    }
}
