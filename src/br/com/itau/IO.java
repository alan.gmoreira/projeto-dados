package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IO {

    private static Scanner scan = new Scanner(System.in);
    private static int qtdJogadores = 0;
    private static int tamanhoDado = 0;
    private static int quantidadeDados = 0;

    public static void IniciaExercicioAula() {

        System.out.println("***********************RESULTADOS***********************");
        System.out.println("EXERCICIO 1: " + new Dado(6, true).getValorJogado());

        List<Dado> dadosExercicio2 = new ArrayList<Dado>();
        dadosExercicio2.add(new Dado(6, true));
        dadosExercicio2.add(new Dado(6, true));
        dadosExercicio2.add(new Dado(6, true));
        GrupoDados grupoDadosExercicio2 = new GrupoDados(dadosExercicio2);
        System.out.println("EXERCICIO 2: " + grupoDadosExercicio2.toString());
                /*+ grupoDadosExercicio2.getDados().get(0).getValorJogado() + ", "
                + grupoDadosExercicio2.getDados().get(1).getValorJogado() + ", "
                + grupoDadosExercicio2.getDados().get(2).getValorJogado() + ", "
                + grupoDadosExercicio2.getSomaDadod());*/


        System.out.println("EXERCICIO 3: ");

        List<Dado> dadosExercicio3_1 = new ArrayList<Dado>();
        dadosExercicio3_1.add(new Dado(6, true));
        dadosExercicio3_1.add(new Dado(6, true));
        dadosExercicio3_1.add(new Dado(6, true));
        GrupoDados grupoDadosExercicio3_1 = new GrupoDados(dadosExercicio3_1);

        List<Dado> dadosExercicio3_2 = new ArrayList<Dado>();
        dadosExercicio3_2.add(new Dado(6, true));
        dadosExercicio3_2.add(new Dado(6, true));
        dadosExercicio3_2.add(new Dado(6, true));
        GrupoDados grupoDadosExercicio3_2 = new GrupoDados(dadosExercicio3_2);

        List<Dado> dadosExercicio3_3 = new ArrayList<Dado>();
        dadosExercicio3_3.add(new Dado(6, true));
        dadosExercicio3_3.add(new Dado(6, true));
        dadosExercicio3_3.add(new Dado(6, true));
        GrupoDados grupoDadosExercicio3_3 = new GrupoDados(dadosExercicio3_3);

        System.out.println(grupoDadosExercicio3_1.toString());
        System.out.println(grupoDadosExercicio3_2.toString());
        System.out.println(grupoDadosExercicio3_3.toString());
    }

    public static void IniciarJogo() throws Exception {
        //METODO NÃO TESTADO

        System.out.println("Selecine a quantidade de jogadores");
        qtdJogadores = scan.nextInt();
        System.out.println("Selecine o tamanho dos dados");
        tamanhoDado = scan.nextInt();
        System.out.println("Selecine a quantidade de dados por jogarod");
        quantidadeDados = scan.nextInt();

        List<Jogador> jogadores = solicitaDadosDosJogadores();

        Tabuleiro tabuleiro = new Tabuleiro(jogadores);

        System.out.println("Vencedor: " + tabuleiro.retornaJogadorVencedor());

        //METODO NÃO TESTADO
    }

    private static List<Jogador> solicitaDadosDosJogadores() {
        List<Jogador> retornoJogadores = new ArrayList<Jogador>();
        Jogador jogador;

        for (int i = 0; i < qtdJogadores; i++) {
            System.out.println("Digite o nome:");
            jogador = new Jogador(i, scan.next());
            jogador.JogarDados(quantidadeDados, tamanhoDado);
            retornoJogadores.add(jogador);
        }
        return retornoJogadores;
    }

}
