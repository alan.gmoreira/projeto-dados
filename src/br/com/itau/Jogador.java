package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogador {
    private int id;
    private String nome;
    private List<GrupoDados> grupoDados;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<GrupoDados> getGrupoDados() {
        return grupoDados;
    }

    public void setGrupoDados(List<GrupoDados> grupoDados) {
        this.grupoDados = grupoDados;
    }

    public Jogador(int id, String nome) {
        this.id = id;
        this.nome = nome;
        this.grupoDados = new ArrayList<GrupoDados>();
    }

    public void JogarDados(int quantidadeDados, int tamanhoDado) {
        for (GrupoDados gd : this.getGrupoDados()) {
            gd.JogarDados(quantidadeDados, tamanhoDado);
        }
    }

    public int getPontuacaoTotal() {
        int total = 0;
        for (GrupoDados gd : this.grupoDados
        ) {
            total += gd.getSomaDadod();
        }

        return total;
    }

    @Override
    public String toString() {
        String retorno = "";

        retorno += this.nome;

        for (GrupoDados gd : this.grupoDados
        ) {
            retorno += gd.getSomaDadod() + ", ";
        }

        return retorno;
    }
}
